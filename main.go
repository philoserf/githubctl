package main

import (
	"flag"
	"log"
)

var (
	command string
	target  string
	stars   bool
	org     bool
)

func init() {
	flag.StringVar(&command, "command", "clone-all", "The githubctl command")
	flag.StringVar(&target, "target", "none", "the github user or organization")
	flag.BoolVar(&stars, "stars", false, "bool")
	flag.BoolVar(&org, "org", false, "bool")
	flag.Parse()
}

func main() {
	log.Printf("command: %s target: %s stars: %t", command, target, stars)
}
